## Tipple Cocktails by Mark Graham

#### Introduction

Simple React app using `localStorage` and `material-ui` for some visual flair. 

##### Trade-offs

If this were a production service I would be using more persistence data storage, HoCs to provide data functionality to components and better structuring of individual components and containers.

I also didn't have enough time available to add test coverage or ratings functionality.

#### Quick start

Installation:

```
npm install
npm start
```

#### Stack

##### Language used
React v16.6.0

##### Dependencies
material-ui, node-sass, lodash
