import React from 'react';
import Paper from '@material-ui/core/Paper';
import Navigation from '../components/Navigation';
import SearchTab from '../components/SearchTab';
import FavouritesTab from '../components/FavouritesTab';
import DataStoreService from '../services/DataStoreService';

import './main.scss';

class Main extends React.Component {
  state = {
    tab: 0,
    favourites: DataStoreService.getData('user_data', 'favourites', [])
  };

  handleTabChange = (tab) => {
    this.setState({ tab });
  };

  handleFavouritesChange = (favourites) => {
    this.setState({ favourites });
  };

  renderSearchTab = () => {
    return <SearchTab handleFavouritesChange={this.handleFavouritesChange} />;
  };

  renderFavouritesTab = () => {
    return (
      <FavouritesTab
        favourites={this.state.favourites}
        handleFavouritesChange={this.handleFavouritesChange}
      />
    );
  };

  render() {
    return (
      <div>
        <Navigation favourites={this.state.favourites.length} handleTabChange={this.handleTabChange} />
        <div className="content-box">
          <Paper>
            {this.state.tab === 0 && this.renderSearchTab()}
            {this.state.tab === 1 && this.renderFavouritesTab()}
          </Paper>
        </div>
      </div>
    );
  }
}

export default Main;
