import React from 'react';
import PropTypes from 'prop-types';
import CocktailGrid from "./CocktailGrid";
import DataStoreService from "../services/DataStoreService";

class FavouritesTab extends React.Component {
  handleDelete = (cocktail) => {
    const store = DataStoreService.removeDataByCriteria('user_data', 'favourites', (target) =>
      cocktail.idDrink === target.idDrink
    );
    this.props.handleFavouritesChange(store);
  };

  render() {
    const { favourites } = this.props;
    return (
      <div>
        {favourites.length !== 0 && (
          <CocktailGrid
            cocktails={favourites}
            handleAction={this.handleDelete}
          />
        )}
      </div>
    );
  }
}

FavouritesTab.propTypes = {
  favourites: PropTypes.array.isRequired,
  handleFavouritesChange: PropTypes.func.isRequired,
};

export default FavouritesTab;
