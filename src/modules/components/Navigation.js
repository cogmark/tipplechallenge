import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Badge from '@material-ui/core/Badge';

const styles = {
  root: {
    flexGrow: 1,
  },
  padding: {
    padding: `0 12px`,
  },
};

class Navigation extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.props.handleTabChange(value);
    this.setState({ value });
  };

  render() {
    const { favourites, classes } = this.props;

    return (
      <Paper className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="Search Cocktails" />
          <Tab
            label={
              <Badge className={classes.padding} color="secondary" badgeContent={favourites}>
                Favourites
              </Badge>
            }
          />
        </Tabs>
      </Paper>
    );
  }
}

Navigation.propTypes = {
  handleTabChange: PropTypes.func.isRequired,
  favourites: PropTypes.number.isRequired
};

export default withStyles(styles)(Navigation);
