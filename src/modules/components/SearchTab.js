import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import DataStoreService from '../services/DataStoreService';

import CocktailGrid from './CocktailGrid';

import './searchTab.scss';

const endpoint = search => `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${search}`;

class SearchTab extends React.Component {
  state = {
    search: null,
    cocktailResults: false,
    snackbar: false
  };

  handleTextChange = (event) => {
    this.setState({ search: event.target.value });
  };

  handleSnackbarOpen = (message) => {
    this.setState({ snackbar: message });
  };

  handleSnackbarClose = () => {
    this.setState({ snackbar: false });
  };

  handleFavourite = (cocktail) => {
    const store = DataStoreService.prependData('user_data', 'favourites', cocktail);
    this.handleSnackbarOpen(`${cocktail.strDrink} has been added to your favourites!`);
    this.props.handleFavouritesChange(store);
  };

  handleSearch = () => {
    fetch(endpoint(this.state.search))
      .then((response) => {
        return response.json();
      })
      .then((cocktails) => {
        this.setState({
          cocktailResults: cocktails.drinks
        });
      });
  };

  render() {
    return (
      <div>
        <TextField
          id="outlined-search"
          label="Search field"
          type="search"
          margin="normal"
          variant="outlined"
          onChange={this.handleTextChange}
        />
        <div className="button">
          <Button variant="contained" color="primary" className="button" onClick={this.handleSearch}>
            Search {this.state.search}
          </Button>
        </div>
        {this.state.cocktailResults
          && <CocktailGrid cocktails={this.state.cocktailResults} handleAction={this.handleFavourite} />
        }
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={this.state.snackbar !== false}
          onClose={this.handleSnackbarClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{ this.state.snackbar }</span>}
        />
      </div>
    );
  }
}

SearchTab.propTypes = {
  handleFavouritesChange: PropTypes.func.isRequired,
};

export default SearchTab;
