import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid/Grid';
import CocktailCard from "./CocktailCard";

const CocktailGrid = (props) => {
  const { cocktails, handleAction } = props;

  return (
    <Grid
      container
      direction="row"
      justify="space-evenly"
      alignItems="flex-start"
    >
      {cocktails.map(cocktail => (
        <Grid item key={cocktail.idDrink}>
          <CocktailCard
            key={cocktail.idDrink}
            addToFavourites={handleAction}
            cocktailId={cocktail.idDrink}
            title={cocktail.strDrink}
            text={cocktail.strInstructions}
            image={cocktail.strDrinkThumb}
          />
        </Grid>
      ))}
    </Grid>
  );
};

CocktailGrid.propTypes = {
  cocktails: PropTypes.array.isRequired,
  handleAction: PropTypes.func.isRequired,
};

export default CocktailGrid;
