import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: 345,
    display: 'inline-block'
  },
  media: {
    height: 140,
  },
};

const CocktailCard = (props) => {
  const { classes, title, text, image, addToFavourites, cocktailId } = props;

  const cocktail = {
    idDrink: cocktailId,
    strDrink: title,
    strInstructions: text,
    strDrinkThumb: image,
  };

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={() => addToFavourites(cocktail)}>
        <CardMedia
          className={classes.media}
          image={image}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            { title }
          </Typography>
          <Typography component="p">
            { text }
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

CocktailCard.propTypes = {
  classes: PropTypes.object.isRequired,
  addToFavourites: PropTypes.func.isRequired,
  cocktailId: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string,
  image: PropTypes.string,
};

export default withStyles(styles)(CocktailCard);
