import get from 'lodash.get';
import set from 'lodash.set';

export default class DataStoreService {
  static setItem(key, path, data) {
    const storedItem = window.localStorage.getItem(key) || '{}';
    const storedObject = JSON.parse(storedItem);
    set(storedObject, path, data);
    window.localStorage.setItem(key, JSON.stringify(storedObject));
  }

  static getData(key, path, defaultValue) {
    const item = window.localStorage.getItem(key) || 'null';
    return get(JSON.parse(item), path, defaultValue);
  }

  static prependData(key, path, data) {
    const existingItem = this.getData(key, path, []);
    existingItem.unshift(data);
    this.setItem(key, path, existingItem);
    return existingItem;
  }

  static removeDataByCriteria(key, path, callback) {
    const existingItem = this.getData(key, path, []);
    const targetIndex = existingItem.findIndex(item => callback(item));
    existingItem.splice(targetIndex, 1);
    this.setItem(key, path, existingItem);
    return existingItem;
  }
}
